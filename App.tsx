import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { AppNavigator } from './src/navigators';
import linking from './src/utils/linking';
import './src/utils/i18n';

const App = () => (
  <SafeAreaProvider>
    <NavigationContainer linking={linking}>
      <AppNavigator />
    </NavigationContainer>
  </SafeAreaProvider>
);

export default App;
