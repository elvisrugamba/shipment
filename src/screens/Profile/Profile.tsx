import React from 'react';
import { View, ScrollView, Image, StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';

import Text from '../../components/Text';
import theme from '../../constants/theme';
import styles from './Profile.styles';

const Profile: React.FC = () => {
  const { t } = useTranslation();

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={theme.COLORS.WHITE} />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.content}>
          <View style={styles.heading}>
            <Image
              source={require('../../../assets/profile.png')}
              style={styles.thumbnail}
            />
            <Text size={24}>Elvis Rugamba</Text>
            <Text bold size={14}>
              Software Engineer
            </Text>
          </View>
          <View style={styles.main}>
            <View style={styles.item}>
              <Text bold size={20} style={styles.label}>
                {t('about')}
              </Text>
              <Text>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce
                ultrices lorem sit amet feugiat molestie. Praesent id imperdiet
                neque. Proin aliquam eros ac est consectetur, vitae vehicula mi
                dapibus. Fusce sollicitudin ligula arcu, vitae egestas dolor
                imperdiet et.Nam et ex non purus gravida suscipit. Fusce
                vestibulum tortor in nunc pulvinar dignissim
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Profile;
