import React from 'react';
import { View, ScrollView, Image, StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';

import { HomeStackScreenProps } from '../../types/navigator.types';
import Text from '../../components/Text';
import theme from '../../constants/theme';
import styles from './Details.styles';

type DetailsProps = {
  route: HomeStackScreenProps<'Details'>['route'];
};

interface PickupServices {
  constructionSite: boolean;
  courierService: boolean;
  drayageService: boolean;
  droppedTrailer: boolean;
  insideService: boolean;
}

const Details: React.FC<DetailsProps> = ({ route }) => {
  const { t } = useTranslation();

  const findByKey = (obj: object) =>
    Object.keys(obj).filter((key: string) => obj[key]);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={theme.COLORS.WHITE} />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.content}>
          <View style={styles.heading}>
            <Text size={24}>Details</Text>
          </View>
          <View style={styles.main}>
            <View style={styles.item}>
              <Text bold size={18} style={styles.label}>
                {t('shipper')}
              </Text>
              <Text>{route.params?.items?.shipper}</Text>
            </View>
            <View style={styles.item}>
              <Text bold size={18} style={styles.label}>
                {t('location')}
              </Text>
              <Text>{route.params?.items?.location}</Text>
            </View>
            <View style={styles.item}>
              <Text bold size={18} style={styles.label}>
                {t('bol')}
              </Text>
              <Text>{route.params?.items?.bol}</Text>
            </View>
            <View style={styles.item}>
              <Text bold size={18} style={styles.label}>
                {t('serviceMode')}
              </Text>
              <Text>{route.params?.items?.serviceMode}</Text>
            </View>
            <View style={styles.item}>
              <Text bold size={18} style={styles.label}>
                {t('transitService')}
              </Text>
              <Text>{route.params?.items?.transitService}</Text>
            </View>
            <View style={styles.item}>
              <Text bold size={18} style={styles.label}>
                {t('pickupServices')}
              </Text>
              {findByKey(route.params?.items?.pickupServices).map(
                (val: any) => (
                  <Text>{t(val)}</Text>
                ),
              )}
            </View>
            <View style={styles.item}>
              <Text bold size={18} style={styles.label}>
                {t('datePickupRequested')}
              </Text>
              <Text>
                {route.params?.items?.datePickupRequested?.toLocaleDateString()}
              </Text>
            </View>
            <View style={styles.item}>
              <Text bold size={18} style={styles.label}>
                {t('datePickupActual')}
              </Text>
              <Text>
                {route.params?.items?.datePickupActual?.toLocaleDateString()}
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Details;
