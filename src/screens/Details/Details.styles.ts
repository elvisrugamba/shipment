import { StyleSheet } from 'react-native';

import theme from '../../constants/theme';

const styles = StyleSheet.create({
  flex: {
    ...theme.STYLES.flex,
  },
  container: {
    ...theme.STYLES.flex,
    backgroundColor: theme.COLORS.BACKGROUND,
  },
  content: {
    ...theme.STYLES.flex,
    backgroundColor: theme.COLORS.BACKGROUND,
  },
  heading: {
    padding: theme.SIZES.SPACING,
    backgroundColor: theme.COLORS.GRAY,
  },
  thumbnail: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginBottom: 4,
  },
  main: {
    paddingHorizontal: theme.SIZES.SPACING,
  },
  item: {
    marginVertical: theme.SIZES.INPUT_SPACING,
  },
  label: {
    marginBottom: 4,
  },
});

export default styles;
