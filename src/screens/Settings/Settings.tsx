import React, { useState } from 'react';
import { View, ScrollView, StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';

import Dropdown from '../../components/Dropdown';
import theme from '../../constants/theme';
import styles from './Settings.styles';

const LANGS = [
  { key: 1, value: 'en', label: 'English' },
  { key: 2, value: 'kn', label: 'Kinyarwanda' },
];

const Settings: React.FC = () => {
  const [language, setLanguage] = useState('en');
  const { t, i18n } = useTranslation();

  const changeLanguage = (lng: string) => {
    setLanguage(lng);
    i18n.changeLanguage(lng);
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={theme.COLORS.WHITE} />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.content}>
          <View style={styles.form}>
            <View style={styles.formRow}>
              <Dropdown
                label={t('changeLanguage')}
                options={LANGS}
                selectedValue={language}
                onValueChange={val => changeLanguage(val)}
                style={styles.dropdown}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Settings;
