import { StyleSheet } from 'react-native';

import theme from '../../constants/theme';

const styles = StyleSheet.create({
  flex: {
    ...theme.STYLES.flex,
  },
  container: {
    ...theme.STYLES.flex,
    backgroundColor: theme.COLORS.BACKGROUND,
  },
  content: {
    ...theme.STYLES.flex,
    backgroundColor: theme.COLORS.BACKGROUND,
  },
  form: {
    paddingHorizontal: theme.SIZES.SPACING,
  },
  formRow: {
    ...theme.STYLES.row,
    ...theme.STYLES.center,
    justifyContent: 'space-between',
    marginTop: 16,
    marginBottom: theme.SIZES.INPUT_SPACING,
  },
  dropdown: {
    width: theme.SIZES.windowWidth - theme.SIZES.SPACING * 2,
  },
});

export default styles;
