import React, { useState } from 'react';
import { View, ScrollView, StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { HomeStackScreenProps } from '../../types/navigator.types';
import { FormValues } from '../../interfaces/form-values.interface';
import Header from '../../components/Header';
import Text from '../../components/Text';
import Stepper from '../../components/Stepper';
import Input from '../../components/Input';
import Dropdown from '../../components/Dropdown';
import CheckBoxInput from '../../components/CheckBoxInput';
import DatePicker from '../../components/DatePicker';
import Button from '../../components/Button';
import theme from '../../constants/theme';
import styles from './Home.styles';

const STEPS = 6;
const FormSchema = Yup.object().shape({
  shipper: Yup.string()
    .min(2, 'Shipper is too short')
    .max(50, 'Shipper is too long')
    .required('Shipper is required'),
  location: Yup.string()
    .min(2, 'Location is too short')
    .max(50, 'Location is too long')
    .required('Location is required'),
  bol: Yup.string(),
  serviceMode: Yup.string().required('Service Mode is required'),
  transitService: Yup.string().required('Transit Service is required'),
  datePickupRequested: Yup.date().required('Date Pickup Requested is required'),
  datePickupActual: Yup.date().required('Date Pickup Actual is required'),
});

type HomeProps = {
  navigation: HomeStackScreenProps<'Home'>;
};

const Home: React.FC<HomeProps> = ({ navigation }) => {
  const [currentStep, setCurrentStep] = useState(1);
  const { t } = useTranslation();
  const {
    values,
    handleChange,
    handleBlur,
    setFieldValue,
    touched,
    errors,
    handleSubmit,
  } = useFormik<FormValues>({
    initialValues: {
      shipper: '',
      location: '',
      bol: '',
      serviceMode: 'LTL',
      transitService: '',
      pickupServices: {
        constructionSite: false,
        courierService: false,
        drayageService: false,
        droppedTrailer: false,
        insideService: false,
      },
      datePickupRequested: undefined,
      datePickupActual: undefined,
    },
    validationSchema: FormSchema,
    onSubmit: formValues => {
      // alert(JSON.stringify(formValues, null, 2));
      next(formValues);
    },
  });

  const next = (items: FormValues) => {
    // if (currentStep < STEPS) {
    //   setCurrentStep(currentStep + 1);
    // }
    navigation.navigate('Details', { items });
  };

  const prev = () => {
    if (currentStep > 1) {
      setCurrentStep(currentStep - 1);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={theme.COLORS.WHITE} />
      <Header leftHandler={() => navigation.openDrawer()} />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.content}>
          <View style={styles.heading}>
            <Text size={24}>{t('createShipment')}</Text>
            <Text bold size={14}>
              {t('steps', { current: currentStep, total: STEPS })}
            </Text>
            <Text size={14}>{t('step', { count: currentStep })}</Text>
          </View>
          <View>
            <Stepper steps={STEPS} current={currentStep} />
          </View>
          <View style={styles.form}>
            <View style={styles.formHeader}>
              <Text size={14} color={theme.COLORS.ERROR}>
                *
              </Text>
              <Text>{t('indicatesRequiredField')}</Text>
            </View>
            <Input
              label={t('shipper')}
              placeholder={t('companyName')}
              required
              value={values.shipper}
              onChangeText={handleChange('shipper')}
              onBlur={handleBlur('shipper')}
            />
            <Input
              label={t('location')}
              placeholder={t('address')}
              required
              style={styles.noBorderInput}
              value={values.location}
              onChangeText={handleChange('location')}
              onBlur={handleBlur('location')}
            />
            {touched.shipper && errors.shipper ? (
              <Text size={14} color={theme.COLORS.ERROR}>
                {errors.shipper}
              </Text>
            ) : null}
            {touched.location && errors.location ? (
              <Text size={14} color={theme.COLORS.ERROR}>
                {errors.location}
              </Text>
            ) : null}
            <Input
              label={t('bol')}
              placeholder={t('optional')}
              style={styles.optionalInput}
              value={values.bol}
              onChangeText={handleChange('bol')}
              onBlur={handleBlur('bol')}
            />
            <View style={styles.formRow}>
              <Dropdown
                label={t('serviceMode')}
                options={[
                  { key: 1, label: 'LTL', value: 'LTL' },
                  { key: 2, label: 'FTL', value: 'FTL' },
                ]}
                style={styles.dropdown}
                selectedValue={values.serviceMode}
                onValueChange={val => setFieldValue('serviceMode', val)}
                onBlur={handleBlur('serviceMode')}
                error={
                  touched.serviceMode && errors.serviceMode
                    ? errors.serviceMode
                    : undefined
                }
              />
              <Dropdown
                label={t('transitService')}
                options={[
                  { key: 1, label: t('selectOne'), value: null },
                  { key: 2, label: 'First', value: 'first' },
                  { key: 3, label: 'Second', value: 'second' },
                ]}
                style={styles.dropdown}
                selectedValue={values.transitService}
                onValueChange={val => setFieldValue('transitService', val)}
                onBlur={handleBlur('transitService')}
                error={
                  touched.transitService && errors.transitService
                    ? errors.transitService
                    : undefined
                }
              />
            </View>
            <View style={styles.services}>
              <Text bold style={styles.servicesLabel}>
                {t('pickupServices')}
              </Text>
              <CheckBoxInput
                label={t('constructionSite')}
                value={values.pickupServices.constructionSite}
                onValueChange={val =>
                  setFieldValue('pickupServices.constructionSite', val)
                }
              />
              <CheckBoxInput
                label={t('courierService')}
                value={values.pickupServices.courierService}
                onValueChange={val =>
                  setFieldValue('pickupServices.courierService', val)
                }
              />
              <CheckBoxInput
                label={t('drayageService')}
                value={values.pickupServices.drayageService}
                onValueChange={val =>
                  setFieldValue('pickupServices.drayageService', val)
                }
              />
              <CheckBoxInput
                label={t('droppedTrailer')}
                value={values.pickupServices.droppedTrailer}
                onValueChange={val =>
                  setFieldValue('pickupServices.droppedTrailer', val)
                }
              />
              <CheckBoxInput
                label={t('insideService')}
                value={values.pickupServices.insideService}
                onValueChange={val =>
                  setFieldValue('pickupServices.insideService', val)
                }
              />
            </View>
            <View style={styles.formRow}>
              <DatePicker
                label={t('datePickupRequested')}
                style={styles.dropdown}
                value={values.datePickupRequested}
                onChange={val => setFieldValue('datePickupRequested', val)}
                error={
                  touched.datePickupRequested && errors.datePickupRequested
                    ? errors.datePickupRequested
                    : undefined
                }
              />
              <DatePicker
                label={t('datePickupActual')}
                style={styles.dropdown}
                value={values.datePickupActual}
                onChange={val => setFieldValue('datePickupActual', val)}
                error={
                  touched.datePickupActual && errors.datePickupActual
                    ? errors.datePickupActual
                    : undefined
                }
              />
            </View>
            <View style={styles.buttons}>
              <Button
                transparent
                style={styles.button}
                textStyle={styles.backBtnText}
                onPress={prev}>
                {t('back')}
              </Button>
              <Button style={styles.button} onPress={handleSubmit}>
                {t('next')}
              </Button>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
