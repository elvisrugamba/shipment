import { LinkingOptions } from '@react-navigation/native';

import { RootDrawerParamList } from '../types/navigator.types';

const config = {
  screens: {
    HomeStack: {
      initialRouteName: 'Home',
      screens: {
        Profile: 'profile/:userName',
      },
    },
    Settings: 'settings',
  },
};

const linking: LinkingOptions<RootDrawerParamList> = {
  prefixes: ['shipment://', 'https://app.shipment.com'],
  config,
};

export default linking;
