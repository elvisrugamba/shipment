export interface FormValues {
  shipper: string;
  location: string;
  bol?: string;
  serviceMode: string;
  transitService: string;
  pickupServices: {
    constructionSite: boolean;
    courierService: boolean;
    drayageService: boolean;
    droppedTrailer: boolean;
    insideService: boolean;
  };
  datePickupRequested: Date | undefined;
  datePickupActual: Date | undefined;
}
