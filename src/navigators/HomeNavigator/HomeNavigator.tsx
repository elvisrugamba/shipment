import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { HomeStackParamList } from '../../types/navigator.types';
import { Home, Profile } from '../../screens';
import Details from '../../screens/Details';

const Stack = createNativeStackNavigator<HomeStackParamList>();

const HomeNavigator = () => (
  <Stack.Navigator initialRouteName="Home">
    <Stack.Screen
      name="Home"
      component={Home}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Profile"
      component={Profile}
      options={{ presentation: 'modal' }}
    />
    <Stack.Screen
      name="Details"
      component={Details}
      options={{ headerShadowVisible: false }}
    />
  </Stack.Navigator>
);

export default HomeNavigator;
