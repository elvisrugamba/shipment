import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import HomeNavigator from '../HomeNavigator';
import { Settings } from '../../screens';
import { RootDrawerParamList } from '../../types/navigator.types';

const Drawer = createDrawerNavigator<RootDrawerParamList>();

const AppNavigator = () => (
  <Drawer.Navigator>
    <Drawer.Screen
      name="HomeStack"
      component={HomeNavigator}
      options={{ drawerLabel: 'Home', headerShown: false }}
    />
    <Drawer.Screen name="Settings" component={Settings} />
  </Drawer.Navigator>
);

export default AppNavigator;
