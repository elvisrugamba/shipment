import React, { useState } from 'react';
import { View, Pressable, ViewProps, ViewStyle, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import Menu from '../Menu';
import theme from '../../constants/theme';
import styles from './Header.styles';

export type HeaderProps = ViewProps & {
  leftHandler: () => void;
  style?: ViewStyle;
};

const Header: React.FC<HeaderProps> = ({ leftHandler, style }) => {
  const [showMenu, setShowMenu] = useState(false);

  return (
    <>
      <View style={[styles.header, style]}>
        <View style={styles.leftView}>
          <Pressable style={styles.button} onPress={leftHandler}>
            <Icon name="menu" size={32} color={theme.COLORS.DEFAULT} />
          </Pressable>
        </View>
        <View style={styles.centerView}>
          <Image
            source={require('../../../assets/logo.png')}
            style={styles.logo}
          />
        </View>
        <View style={styles.rightView}>
          <Pressable style={styles.profile} onPress={() => setShowMenu(true)}>
            <Image
              source={require('../../../assets/profile.png')}
              style={styles.thumbnail}
            />
            <Icon name="caret-down" size={14} color={theme.COLORS.DEFAULT} />
          </Pressable>
        </View>
      </View>
      {showMenu ? (
        <Menu visible={showMenu} close={() => setShowMenu(false)} />
      ) : null}
    </>
  );
};

export default Header;
