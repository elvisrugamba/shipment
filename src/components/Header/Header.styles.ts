import { StyleSheet } from 'react-native';

import theme from '../../constants/theme';

const styles = StyleSheet.create({
  header: {
    height: 65,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: theme.COLORS.WHITE,
    paddingHorizontal: theme.SIZES.SPACING,
  },
  leftView: {
    width: 30,
    height: 30,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  centerView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightView: {
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
  },
  profile: {
    ...theme.STYLES.row,
    ...theme.STYLES.center,
  },
  thumbnail: {
    width: 30,
    height: 30,
    borderRadius: 30,
    marginRight: 4,
  },
  logo: {
    width: 30,
    height: 30,
  },
});

export default styles;
