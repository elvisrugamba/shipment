import { StyleSheet } from 'react-native';

import theme from '../../constants/theme';

const styles = StyleSheet.create({
  item: {
    // backgroundColor: 'red',
  },
  labelView: {
    padding: theme.SIZES.INPUT_SPACING,
  },
  pickerView: {
    height: 45,
    width: '100%',
    backgroundColor: theme.COLORS.PICKER,
    paddingHorizontal: theme.SIZES.INPUT_SPACING,
  },
  picker: {
    ...theme.STYLES.flex,
    color: theme.COLORS.DEFAULT,
    fontSize: 16,
    marginHorizontal: -12,
    marginTop: -6,
  },
  label: {
    fontSize: theme.SIZES.BASE,
    lineHeight: 18,
  },
});

export default styles;
