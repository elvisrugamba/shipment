import React, { useState } from 'react';
import { View, Pressable, Platform, ViewStyle, ViewProps } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import DateTimePicker, { Event } from '@react-native-community/datetimepicker';

import Text from '../Text';
import theme from '../../constants/theme';
import styles from './DatePicker.styles';

export type DatePickerProps = ViewProps & {
  label: string;
  value?: Date;
  onChange?: (date: Date) => void;
  error?: string;
  style?: ViewStyle;
};

const DatePicker: React.FC<DatePickerProps> = ({
  label,
  value,
  onChange,
  error,
  style,
}) => {
  const [date, setDate] = useState(value);
  const [show, setShow] = useState(false);

  const onDateChange = (event: Event, selectedDate: Date) => {
    const currentDate = selectedDate;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    onChange && onChange(currentDate);
  };

  const showDatepicker = () => {
    setShow(true);
  };

  return (
    <View style={[styles.item, style]}>
      <View style={styles.labelView}>
        <Text bold style={styles.label}>
          {label}
        </Text>
      </View>
      <Pressable style={styles.pickerView} onPress={showDatepicker}>
        <Text bold numberOfLines={1} style={styles.picker}>
          {date?.toLocaleDateString() || 'Select Date'}
        </Text>
        <Icon name="caret-down" size={14} color={theme.COLORS.DEFAULT} />
      </Pressable>
      {show && (
        <DateTimePicker
          value={new Date()}
          mode="date"
          is24Hour={true}
          display="default"
          onChange={onDateChange}
        />
      )}
      {error ? (
        <Text size={14} color={theme.COLORS.ERROR}>
          {error}
        </Text>
      ) : null}
    </View>
  );
};

export default DatePicker;
