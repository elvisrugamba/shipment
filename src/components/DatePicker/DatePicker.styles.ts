import { StyleSheet } from 'react-native';

import theme from '../../constants/theme';

const styles = StyleSheet.create({
  item: {
    // backgroundColor: 'red',
  },
  labelView: {
    padding: theme.SIZES.INPUT_SPACING,
    paddingRight: 32,
  },
  pickerView: {
    ...theme.STYLES.row,
    ...theme.STYLES.center,
    justifyContent: 'space-between',
    height: 45,
    width: '100%',
    backgroundColor: theme.COLORS.PICKER,
    paddingHorizontal: theme.SIZES.INPUT_SPACING,
  },
  picker: {
    color: theme.COLORS.DEFAULT,
    fontSize: 16,
  },
  label: {
    fontSize: theme.SIZES.BASE,
    lineHeight: 18,
  },
});

export default styles;
