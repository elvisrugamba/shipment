import { StyleSheet } from 'react-native';

import theme from '../../constants/theme';

const styles = StyleSheet.create({
  backDrop: {
    flex: 1,
    alignItems: 'flex-end',
  },
  modalView: {
    backgroundColor: theme.COLORS.WHITE,
    marginHorizontal: theme.SIZES.SPACING,
    marginTop: 55,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: theme.COLORS.BACKGROUND,
    shadowColor: theme.COLORS.DEFAULT,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.51,
    shadowRadius: 13.16,
    elevation: 20,
  },
  titleView: {
    padding: theme.SIZES.SPACING,
    borderBottomWidth: 1,
    borderBottomColor: theme.COLORS.GRAY,
  },
  content: {
    paddingVertical: 8,
  },
  button: {
    justifyContent: 'center',
    padding: theme.SIZES.SPACING,
    paddingVertical: 8,
  },
});

export default styles;
