import React from 'react';
import { View, Modal, Pressable, TouchableOpacity, Share } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { HomeStackScreenProps } from '../../types/navigator.types';
import Text from '../Text';
import theme from '../../constants/theme';
import styles from './Menu.styles';

type MenuProps = {
  visible: boolean;
  close: () => void;
};

const Menu: React.FC<MenuProps> = ({ visible, close }) => {
  const navigation = useNavigation<HomeStackScreenProps<'Home'>>();

  const navigate = (navigateTo: string) => {
    navigation.navigate(navigateTo);
    close();
  };

  const share = async () => {
    try {
      await Share.share({
        title: 'Share profile',
        message: 'https://app.shipment.com/profile/elvis',
      });
      close();
    } catch (error) {
      if (error instanceof Error) {
        console.log(error.message);
      }
    }
  };

  return (
    <Modal visible={visible} transparent={true} onRequestClose={() => close()}>
      <Pressable onPress={close} style={styles.backDrop}>
        <View style={styles.modalView}>
          <View style={styles.titleView}>
            <Text bold>Elvis Rugamba</Text>
          </View>
          <View style={styles.content}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.button}
              onPress={() => navigate('Profile')}>
              <Text color={theme.COLORS.DEFAULT}>My Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.button}
              onPress={share}>
              <Text color={theme.COLORS.DEFAULT}>Share Profile</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Pressable>
    </Modal>
  );
};

export default Menu;
