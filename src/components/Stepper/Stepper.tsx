import React, { Fragment, useState, useEffect } from 'react';
import { View, ViewProps, ViewStyle } from 'react-native';

import Text from '../Text';
import styles from './Stepper.styles';

export type StepperProps = ViewProps & {
  current: number;
  steps: number;
  style?: ViewStyle;
};

const Stepper: React.FC<StepperProps> = ({ current, steps, style }) => {
  const [currentStep, setCurrentStep] = useState(0);

  useEffect(() => {
    setCurrentStep(current);
  }, [current]);

  return (
    <View style={[styles.container, style]}>
      {Array(steps)
        .fill(null)
        .map((_, index) => (
          <Fragment key={index}>
            <View
              style={[
                styles.step,
                currentStep >= index + 1
                  ? styles.activeStep
                  : styles.inactiveStep,
              ]}>
              <Text
                style={[
                  styles.stepText,
                  currentStep >= index + 1
                    ? styles.activeStepText
                    : styles.inactiveStepText,
                ]}>
                {index + 1}
              </Text>
            </View>
            {steps > index + 1 ? (
              <View
                style={[
                  styles.line,
                  currentStep > index + 1
                    ? styles.activeLine
                    : styles.inactiveLine,
                ]}
              />
            ) : null}
          </Fragment>
        ))}
    </View>
  );
};

export default Stepper;
