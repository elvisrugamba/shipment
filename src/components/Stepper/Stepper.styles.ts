import { StyleSheet } from 'react-native';
import theme from '../../constants/theme';

const styles = StyleSheet.create({
  container: {
    ...theme.STYLES.row,
    ...theme.STYLES.center,
    padding: theme.SIZES.SPACING,
  },
  step: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    borderRadius: 30,
    padding: 4,
  },
  stepText: {
    fontSize: 14,
  },
  activeStep: {
    backgroundColor: theme.COLORS.PRIMARY,
  },
  inactiveStep: {
    borderWidth: 1,
    borderColor: theme.COLORS.LIGHT_GRAY,
  },
  activeStepText: {
    color: theme.COLORS.WHITE,
  },
  inactiveStepText: {
    color: theme.COLORS.LIGHT_GRAY,
  },
  line: {
    flex: 1,
    height: 1,
  },
  activeLine: {
    backgroundColor: theme.COLORS.PRIMARY,
  },
  inactiveLine: {
    backgroundColor: theme.COLORS.LIGHT_GRAY,
  },
});

export default styles;
